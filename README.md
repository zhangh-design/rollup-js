# rollup-js

rollup 构建打包 JavaScript 库基础框架（不支持 Vue 文件打包）

> 本框架不能用于特定依赖包的构建，特定依赖包请下载本框架在去进行指定库的打包

```
node >= v12.18.3
```

打包指令：

```
npm run build
```

eslint 校验指令

```
npm run lint
```

##### 注意：

打包 cjs 库的时候请创建一个单独的 input 入口文件对应 frame.config.js 中的 cjsInput 参数，比如：`index-cjs.js` 用于 cjs 库的打包入口文件

构建出的文件对应使用的环境

```
bundle.cjs.js -- Webpack
bundle.cjs.min.js -- Webpack
bundle.esm-browser.js -- <script type=module>
bundle.esm-browser.min.js -- <script type=module>
bundle.esm-bundler.js -- <script type=module>
bundle.global.js -- <script>
bundle.global.min.js -- <script>
```

源码构建输出如下格式:

- IIFE: 自执行函数, 可通过 `<script>` 标签加载
- AMD: 浏览器端的模块规范, 可通过 RequireJS 可加载
- CommonJS: Node 默认的模块规范, 可通过 Webpack 加载
- UMD: 兼容 IIFE, AMD, CJS 三种模块规范
- ESM: ES2015 Module 规范, 可用 Webpack, Rollup 加载
