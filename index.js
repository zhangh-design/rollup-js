if (process.env.NODE_ENV === 'production') {
  module.exports = require('./dist/bundle.cjs.min.js');
} else {
  module.exports = require('./dist/bundle.cjs.js');
}
