'use strict';
/**
 * rollup 打包自定义扩展配置文件
 */
module.exports = {
  input: './src/index.js', // amd、esm、iife （umd模式未构建，umd拆分为amd、cjs 和 iife）
  cjsInput: './src/index-cjs.js', // cjs
  outputFileNamePre: 'bundle', // 构建出文件的前缀名称
  // 构建时移除的第三方包配置
  cjs: {
    external: ['lodash/max'],
    globals: { _max: 'lodash/max' }
  },
  es: {
    bundle: {
      external: ['lodash/max'],
      globals: { _max: 'lodash/max' }
    },
    browser: {
      external: [],
      globals: {}
    }
  },
  iife: {
    outputName: 'MyBundle', // 插件的对外全局变量 （iife 使用这个变量）,
    external: [],
    globals: {}
  }
};
