/* import foo from './foo.js';
export default function () {
  console.log(foo);
} */
import _max from 'lodash/max';
// cjs 打包的入口文件
// eslint-disable-next-line import/no-relative-parent-imports
import { version } from '../package.json';

const testFn = function() {
  // eslint-disable-next-line no-unused-vars
  const b = '';
  const hello = 'hello world';
  const aFruitList = ['apple', 'banana', 'pear', 'pineapple'];
  for (const [index, elem] of aFruitList.entries()) {
    console.log(index, elem);
  }
  const P1 = new Promise(function(resolve, reject) {
    setTimeout(() => {
      resolve('hello-promise');
    }, 3000);
  });
  P1.then(result => {
    return console.log(result);
  }).catch(error => {
    throw new Error(error);
  });
  console.log('version ' + version + hello);
  console.warn('调用lodash的max函数：', _max([4, 2, 8, 6]));
};

// cjs 模式不建议使用 default 导出
export { testFn };
