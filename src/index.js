import _max from 'lodash/max';
import { version } from '../package.json';
import foo from './foo.js';
/* export default function () {
  console.log(foo);
} */
// 除 cjs 模式外的其余模式打包入口
// eslint-disable-next-line import/no-relative-parent-imports

const testFn = function() {
  console.log(foo);
  // eslint-disable-next-line no-unused-vars
  const b = '';
  const hello = 'hello world';
  const aFruitList = ['apple', 'banana', 'pear', 'pineapple'];
  for (const [index, elem] of aFruitList.entries()) {
    console.log(index, elem);
  }
  const P1 = new Promise(function(resolve, reject) {
    setTimeout(() => {
      resolve('hello-promise');
    }, 3000);
  });
  P1.then(result => {
    return console.log(result);
  }).catch(error => {
    throw new Error(error);
  });
  console.log('version ' + version + hello);
  console.warn('调用lodash的max函数：', _max([4, 2, 8, 6]));
};

export default testFn;
